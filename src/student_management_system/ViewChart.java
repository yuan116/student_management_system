package student_management_system;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ViewChart extends JFrame implements ActionListener {

    private JButton backButton;
    public static int sizeOfCodeArray = 0;

    public ViewChart() {
        setTitle("Chart Report");
        setSize(800, 500);
        setLocation(500, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(false);
        setLayout(new BorderLayout());

        generateCodeArray();

        backButton = new JButton("Back");
        backButton.addActionListener(this);

        add(backButton, BorderLayout.SOUTH);
        add(new DisplayPanel(), BorderLayout.CENTER);
    }

    public void generateCodeArray() {
        Student_management_system.getStudentDataFromFile();

        int size = Student_management_system.allStudent.size();
        Student_management_system.codeArray = new String[size];
        Student_management_system.countCodeArray = new int[size];

        sizeOfCodeArray = 0;
        for (int row = 0; row < size; row++) {
            // need to initialize empty string first
            Student_management_system.codeArray[row] = "";
            Student student = Student_management_system.allStudent.get(row);

            for (int count = 0; count < size; count++) {
                if (!Student_management_system.codeArray[count].isEmpty()) {
                    if (Student_management_system.codeArray[count].equals(student.getCourseCode())) {
                        Student_management_system.countCodeArray[count]++;
                        break;
                    }
                } else {
                    Student_management_system.codeArray[count] = student.getCourseCode();
                    Student_management_system.countCodeArray[count] = 1;
                    sizeOfCodeArray++;
                    break;
                }
            }
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == backButton) {
            Student_management_system.chartPage.setVisible(false);
            Student_management_system.dashboardPage.setVisible(true);
        }
    }
}

class DisplayPanel extends JPanel {
    public void paintComponent(Graphics g) {
        int length = ViewChart.sizeOfCodeArray;
        super.paintComponent(g);

        if (length == 0) {
            g.setFont(new Font("Arial", Font.BOLD, 60));
            g.drawString("No Record Found", 165, 250);
        } else {
            g.setFont(new Font("Arial", Font.BOLD, 20));
            g.drawString("Course Code (number of student)", 50, 30);

            g.setColor(Color.red);
            g.drawLine(130, 50, 130, (length+1) * 70);

            for (int count = 0; count < length; count++) {
                g.setColor(Color.blue);
                g.fillRect(131, 63 * (count+1), 40 * Student_management_system.countCodeArray[count], 50);

                g.setColor(Color.black);
                g.setFont(new Font("Arial", Font.BOLD, 15));
                g.drawString(Student_management_system.codeArray[count], 20, 60 * (count + 1) + 35);
                g.drawString("(" + Student_management_system.countCodeArray[count] + ")", 140 + 40 * Student_management_system.countCodeArray[count], 60 * (count + 1) + 35);
            }
        }
    }
}