package student_management_system;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.Color;
import java.awt.Font;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ManageStudent extends JFrame {

    // for listing
    private JButton addStudentButton, backDashboardButton;
    private int size;
    private JButton editButton[];
    private JButton deleteButton[];
    private JPanel body;

    // for form
    private JLabel displayLabel, studentIdLabel, studentNameLabel, courseCodeLabel;
    private JTextField studentIdTextField, studentNameTextField, courseCodeTextField;
    private JPanel bottomPanel;
    private JButton addButton, updateButton, backButton;

    public ManageStudent(String type) {
        setLocation(500, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(false);

        Student_management_system.getStudentDataFromFile();

        if (Student_management_system.allStudent.isEmpty()) {
            size = 0;
        } else {
            size = Student_management_system.allStudent.size();
            editButton = new JButton[size];
            deleteButton = new JButton[size];
        }

        if ("list".equals(type)) {
            studentList();
        } else if ("add".equals(type)) {
            addStudent();
        } else if ("edit".equals(type)) {
            editStudent();
        }
    }

    private void studentList() {
        setTitle("Manage Student");
        setSize(800, (size == 0 ? 300 : size * 200));
        setLayout(new BorderLayout());

        addStudentButton = new JButton("Add Student");
        addStudentButton.addActionListener(new DashboardListener());

        backDashboardButton = new JButton("Back");
        backDashboardButton.addActionListener(new DashboardListener());

        JPanel topPanel = new JPanel(new GridLayout(1, 2));
        topPanel.add(addStudentButton);
        topPanel.add(backDashboardButton);

        if (size == 0) {
            body = new JPanel(new GridLayout(size + 1, 1));
            JLabel messageLabel = new JLabel("No Student Record Found", JLabel.CENTER);
            messageLabel.setFont(new Font("Verdana", Font.BOLD, 20));
            body.add(messageLabel);
        } else {
            body = new JPanel(new GridLayout(size + 1, 5));

            body.add(new JLabel("Student ID"));
            body.add(new JLabel("Student Name"));
            body.add(new JLabel("Course Code"));
            body.add(new JLabel("Edit"));
            body.add(new JLabel("Delete"));

            for (int count = 0; count < size; count++) {
                Student student = Student_management_system.allStudent.get(count);
                editButton[count] = new JButton("Edit");
                editButton[count].addActionListener(new EditListener());
                deleteButton[count] = new JButton("Delete");
                deleteButton[count].addActionListener(new DeleteListener());

                body.add(new JLabel(student.getStudentId()));
                body.add(new JLabel(student.getStudentName()));
                body.add(new JLabel(student.getCourseCode()));
                body.add(editButton[count]);
                body.add(deleteButton[count]);
            }
        }

        add(topPanel, BorderLayout.NORTH);
        add(body, BorderLayout.CENTER);
    }

    private void addStudent() {
        createStudentFormView("add");
    }

    private void editStudent() {
        createStudentFormView("edit");
    }

    private void createStudentFormView(String type) {
        setTitle("Student Form");
        setSize(500, 500);
        setLayout(new GridLayout(8, 1, 30, 5));
        displayLabel = new JLabel("Student Form", JLabel.CENTER);
        displayLabel.setFont(new Font("Verdana", Font.BOLD, 20));

        studentIdLabel = new JLabel("Student ID");
        studentIdTextField = new JTextField(15);

        studentNameLabel = new JLabel("Student Name");
        studentNameTextField = new JTextField(15);

        courseCodeLabel = new JLabel("Course Code");
        courseCodeTextField = new JTextField(15);

        if ("add".equals(type)) {
            addButton = new JButton("Add");
        } else {
            updateButton = new JButton("Update");
        }
        backButton = new JButton("Back");
        bottomPanel = new JPanel(new GridLayout(1, 2, 30, 0));

        if ("add".equals(type)) {
            bottomPanel.add(addButton);
        } else {
            bottomPanel.add(updateButton);
        }

        bottomPanel.add(backButton);

        if ("add".equals(type)) {
            addButton.addActionListener(new FormListener());
        } else {
            updateButton.addActionListener(new FormListener());
        }
        backButton.addActionListener(new FormListener());

        add(displayLabel);
        add(studentIdLabel);
        add(studentIdTextField);
        add(studentNameLabel);
        add(studentNameTextField);
        add(courseCodeLabel);
        add(courseCodeTextField);
        add(bottomPanel);
    }

    class DashboardListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == addStudentButton) {
                Student_management_system.studentFormPage = new ManageStudent("add");
                Student_management_system.studentListPage.setVisible(false);
                Student_management_system.studentFormPage.setVisible(true);
            } else if (e.getSource() == backDashboardButton) {
                Student_management_system.studentListPage.setVisible(false);
                Student_management_system.dashboardPage.setVisible(true);
            }
        }
    }

    class EditListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            for (int count = 0; count < size; count++) {
                if (e.getSource() == editButton[count]) {
                    Student_management_system.choosenIndex = count;
                    break;
                }
            }

            Student_management_system.studentFormPage = new ManageStudent("edit");
            Student student = Student_management_system.allStudent.get(Student_management_system.choosenIndex);
            Student_management_system.studentFormPage.studentIdTextField.setText(student.getStudentId());
            Student_management_system.studentFormPage.studentNameTextField.setText(student.getStudentName());
            Student_management_system.studentFormPage.courseCodeTextField.setText(student.getCourseCode());
            Student_management_system.studentListPage.setVisible(false);
            Student_management_system.studentFormPage.setVisible(true);
        }
    }

    class DeleteListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            for (int count = 0; count < size; count++) {
                if (e.getSource() == deleteButton[count]) {
                    Student_management_system.choosenIndex = count;
                    break;
                }
            }

            Student_management_system.allStudent.remove(Student_management_system.choosenIndex);
            Student_management_system.writeStudentDataToFile();

            Student_management_system.studentListPage.setVisible(false);
            Student_management_system.studentListPage = new ManageStudent("list");
            Student_management_system.studentListPage.setVisible(true);
        }
    }

    class FormListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == addButton) {
                saveStudentForm();
            } else if (e.getSource() == updateButton) {
                saveStudentForm();
            } else if (e.getSource() == backButton) {
                Student_management_system.studentFormPage.setVisible(false);
                Student_management_system.studentListPage.setVisible(true);
            }
        }

        public void saveStudentForm() {
            String studentId = studentIdTextField.getText();
            String studentName = studentNameTextField.getText();
            String courseCode = courseCodeTextField.getText();

            if (studentId.isEmpty() || studentName.isEmpty() || courseCode.isEmpty()) {
                displayLabel.setText("Please do not leave blank.");
                displayLabel.setForeground(Color.red);
            } else {
                if (size == 0) {
                    Student new_student = new Student(studentId, studentName, courseCode);
                    Student_management_system.allStudent.add(new_student);
                    Student_management_system.writeStudentDataToFile();

                    Student_management_system.studentFormPage.setVisible(false);
                    Student_management_system.studentListPage = new ManageStudent("list");
                    Student_management_system.studentListPage.setVisible(true);
                } else {
                    for (int count = 0; count < size; count++) {
                        Student student = Student_management_system.allStudent.get(count);
                        if (student.checkDuplicateStudent(studentId.trim(), courseCode.trim())) {
                            if (Student_management_system.choosenIndex != count) {
                                displayLabel.setText("Duplicate Student ID and Course Code.");
                                displayLabel.setForeground(Color.red);
                            } else {
                                updateStudentData(student, studentId, studentName, courseCode, count);
                                break;
                            }
                        } else {
                            updateStudentData(student, studentId, studentName, courseCode, count);
                            break;
                        }
                    }
                }
            }
        }

        public void updateStudentData(Student student, String studentId, String studentName, String courseCode, int count){
            student.setStudentId(studentId);
            student.setStudentName(studentName);
            student.setCourseCode(courseCode);
            Student_management_system.allStudent.set(count, student);

            Student_management_system.writeStudentDataToFile();
            Student_management_system.studentFormPage.setVisible(false);
            Student_management_system.studentListPage = new ManageStudent("list");
            Student_management_system.studentListPage.setVisible(true);
        }
    }
}
