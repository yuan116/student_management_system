package student_management_system;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.Font;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Dashboard extends JFrame implements ActionListener {

    private JButton chartButton, studentButton, backButton;

    public Dashboard() {
        setTitle("Dashboard");
        setSize(400, 300);
        setLocation(500, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(false);
        setLayout(new GridLayout(2, 1));

        JLabel titleLabel = new JLabel("Dashboard", JLabel.CENTER);
        titleLabel.setFont(new Font("Verdana", Font.BOLD, 20));

        chartButton = new JButton("View Chart Report");
        studentButton = new JButton("Manage Student");
        backButton = new JButton("Log Out");

        JPanel bottomPanel = new JPanel(new GridLayout(1, 3));
        if ("administrator".equals(Student_management_system.userType)) {
            bottomPanel.add(studentButton);
        }
        bottomPanel.add(chartButton);
        bottomPanel.add(backButton);

        add(titleLabel);
        add(bottomPanel);

        chartButton.addActionListener(this);
        studentButton.addActionListener(this);
        backButton.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        Student_management_system.dashboardPage.setVisible(false);
        if (e.getSource() == studentButton) {
            Student_management_system.studentListPage = new ManageStudent("list");
            Student_management_system.studentListPage.setVisible(true);
        } else if (e.getSource() == chartButton) {
            Student_management_system.chartPage = new ViewChart();
            Student_management_system.chartPage.setVisible(true);
        } else if (e.getSource() == backButton) {
            Student_management_system.loginPage.setVisible(true);
        }
    }
}
