package student_management_system;

public class User {

    private String username, password, userType;

    public User(String username, String password, String userType) {
        this.username = username;
        this.password = password;
        this.userType = userType;
    }

    public boolean checkLoginData(String username, String password){
        boolean result = false;

        if(username.equals(this.username) && password.equals(this.password)){
            result = true;
        }

        return result;
    }

    public String getUserType(){
        return this.userType;
    }
}
