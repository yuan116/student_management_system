package student_management_system;

import java.io.File;
import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.Scanner;

public class Student_management_system {

    public static ArrayList<User> allUser;
    public static ArrayList<Student> allStudent;
    public static Login loginPage;
    public static Dashboard dashboardPage;
    public static ManageStudent studentListPage;
    public static ManageStudent studentFormPage;
    public static ViewChart chartPage;
    public static String userType = "";
    public static int choosenIndex = -1;

    public static String codeArray[];
    public static int countCodeArray[];

    public static void main(String[] args) {
        allUser = new ArrayList<User>();
        try {
            Scanner fileInput = new Scanner(new File("user.txt"));
            while (fileInput.hasNext()) {
                String username = fileInput.nextLine();
                String password = fileInput.nextLine();
                String userType = fileInput.nextLine();
                fileInput.nextLine();

                User user = new User(username, password, userType);
                allUser.add(user);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        getStudentDataFromFile();

        loginPage = new Login();
    }

    public static void getStudentDataFromFile() {
        allStudent = new ArrayList<Student>();
        try {
            Scanner fileInput = new Scanner(new File("student.txt"));
            while (fileInput.hasNext()) {
                String studentId = fileInput.nextLine();
                String studentName = fileInput.nextLine();
                String courseCode = fileInput.nextLine();
                fileInput.nextLine();

                Student student = new Student(studentId, studentName, courseCode);
                allStudent.add(student);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void writeStudentDataToFile() {
        try {
            PrintWriter fileWriter = new PrintWriter("student.txt");
            for (int count = 0; count < allStudent.size(); count++) {
                Student student = allStudent.get(count);
                fileWriter.println(student.getStudentId());
                fileWriter.println(student.getStudentName());
                fileWriter.println(student.getCourseCode());
                fileWriter.println();
            }
            fileWriter.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
