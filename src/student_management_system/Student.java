package student_management_system;

public class Student {

    private String studentId, studentName, courseCode;

    public Student(String studentId, String studentName, String courseCode) {
        this.studentId = studentId.trim();
        this.studentName = studentName.trim();
        this.courseCode = courseCode.trim();
    }

    public boolean checkDuplicateStudent(String studentId, String courseCode){
        boolean result = false;

        if(studentId.equals(this.studentId) && courseCode.equals(this.courseCode)){
            result = true;
        }

        return result;
    }

    public String getStudentId(){
        return this.studentId;
    }

    public String getStudentName(){
        return this.studentName;
    }

    public String getCourseCode(){
        return this.courseCode;
    }

    public void setStudentId(String studentId){
        this.studentId = studentId;
    }

    public void setStudentName(String studentName){
        this.studentName = studentName;
    }

    public void setCourseCode(String courseCode){
        this.courseCode = courseCode;
    }
}
