package student_management_system;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.Font;
import java.awt.Color;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Login extends JFrame implements ActionListener {

    private JLabel displayLabel, usernameLabel, passwordLabel;
    private JTextField usernameTextField, passwordTextField;
    private JPanel bottomPanel;
    private JButton loginButton, closeButton;

    public Login() {
        setTitle("Student Management System");
        setSize(400, 300);
        setLocation(500, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setLayout(new GridLayout(6, 1, 30, 5));

        displayLabel = new JLabel("Student Management System", JLabel.CENTER);
        displayLabel.setFont(new Font("Verdana", Font.BOLD, 20));

        usernameLabel = new JLabel("Username");
        usernameTextField = new JTextField(15);

        passwordLabel = new JLabel("Password");
        passwordTextField = new JTextField(15);

        loginButton = new JButton("Login");
        closeButton = new JButton("Close");
        bottomPanel = new JPanel(new GridLayout(1, 2, 30, 0));
        bottomPanel.add(loginButton);
        bottomPanel.add(closeButton);

        loginButton.addActionListener(this);
        closeButton.addActionListener(this);

        add(displayLabel);
        add(usernameLabel);
        add(usernameTextField);
        add(passwordLabel);
        add(passwordTextField);
        add(bottomPanel);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == loginButton) {
            String username = "", password = "";
            try {
                username = usernameTextField.getText();
                password = passwordTextField.getText();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }

            for (int count = 0; count < Student_management_system.allUser.size(); count++) {
                User user = Student_management_system.allUser.get(count);

                if (user.checkLoginData(username, password)) {
                    Student_management_system.userType = user.getUserType();
                    break;
                }
            }

            if (Student_management_system.userType.isEmpty()) {
                displayLabel.setText("Invalid Username or Password");
                displayLabel.setForeground(Color.red);
            } else {
                Student_management_system.dashboardPage = new Dashboard();

                if ("administrator".equals(Student_management_system.userType)) {
                    Student_management_system.loginPage.setVisible(false);
                    Student_management_system.dashboardPage.setVisible(true);
                } else if ("teacher".equals(Student_management_system.userType)) {
                    Student_management_system.loginPage.setVisible(false);
                    Student_management_system.dashboardPage.setVisible(true);
                } else {
                    displayLabel.setText("Invalid Username or Password");
                    displayLabel.setForeground(Color.red);
                }
            }

        } else if (e.getSource() == closeButton) {
            System.exit(0);
        }
    }
}
